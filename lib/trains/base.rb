# frozen_string_literal: true

require_relative './node'
require_relative './edge'
require_relative './graph'

include Trains

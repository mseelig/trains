# frozen_string_literal: true

module Trains
  class EdgeParseError < RuntimeError; end

  class ZeroDistanceError < RuntimeError; end

  class Edge
    attr_accessor :from, :to, :distance

    def self.parse_edge_string(edge_string)
      # format example: 'AB5'
      raise EdgeParseError unless edge_string.size >= 3

      from = Node.new(edge_string[0])
      to = Node.new(edge_string[1])
      distance = edge_string[2..edge_string.length].to_i
      raise ZeroDistanceError if distance.zero?

      [from, to, distance]
    end

    def initialize(*params)
      raise ArgumentError unless (params.size == 3) || params[0].is_a?(String)

      if params[0].is_a?(String)
        params = self.class.parse_edge_string(params[0])
      else
        return ArgumentError unless params[0].is_a?(Node)
        return ArgumentError unless params[1].is_a?(Node)
        return ArgumentError unless params[2].is_a?(Integer)

        params = [params[0], params[1], params[2]]
      end
      @from = params[0]
      @to = params[1]
      @distance = params[2]
      self
    end

    def copy
      Edge.new(from, to, distance)
    end

    def ==(other)
      (from == other.from) && (to == other.to) # distance ignored since only one edge is allowed between nodes
    end

    def to_s
      "#{from}-#{to}(#{distance})"
    end
  end
end

# frozen_string_literal: true

require_relative './graph_helpers'
require_relative './graph_helpers/algorithms/base'

module Trains
  class DuplicateEdgeError < RuntimeError; end

  class Graph
    include Enumerable
    include GraphHelpers

    attr_accessor :nodes_array, :adjacency_hash

    def initialize
      # Stores node labels as keys and adjacency lists (arrays) of edges as values
      @adjacency_hash = {}
      @nodes_array = []
    end

    def add_edge(edge)
      add_node(edge.from)
      add_node(edge.to)
      add_neighbor(edge)
    end

    def add_edges(edges_string)
      edges = parse_new_edges_string(edges_string)
      edges.each { |edge| add_edge(edge) }
    end

    def add_neighbor(edge)
      raise DuplicateEdgeError if neighboring_edges(edge.from).select { |e| e == edge }.size.positive?

      adjacency_hash[edge.from.label] << edge
    end

    def add_node(node)
      @nodes_array << node unless @nodes_array.include?(node)
      adjacency_hash[node.label] ||= []
    end

    def add_nodes(node_array)
      node_array.each { |node| add_node(node) }
    end

    def edges
      adjacency_hash.values.flatten
    end

    def empty?
      nodes.count.zero?
    end

    def find_edge(orig, dest)
      return nil if neighboring_edges(orig).nil?

      neighboring_edges(orig).select { |edge| edge.to == dest }.first
    end

    def has_node?(node)
      nodes.include?(node)
    end

    def neighboring_edges(node)
      adjacency_hash[node.label]
    end

    def neighboring_nodes(node)
      adjacency_hash[node.label].map(&:to)
    end

    def nodes
      adjacency_hash.keys.collect { |k| nodes_array.select { |n| n.label == k }.first }
    end

    def parse_new_edges_string(edges_string)
      # format example: 'AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7'
      edges_string.split(',').map(&:strip).collect { |edge_string| Edge.new(edge_string) }
    end

    def size
      nodes.count
    end

    def to_s
      edges.collect(&:to_s).join
    end
  end
end

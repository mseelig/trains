# frozen_string_literal: true

require_relative './graph_helpers/route'

module Trains
  class NoDirectPathError < RuntimeError; end

  class NoRouteFoundError < RuntimeError; end

  module GraphHelpers
    def direct_path_length(orig, dest)
      raise NoDirectPathError unless has_direct_path?(orig, dest)

      neighboring_edges(orig).select { |edge| dest == edge.to }.first.distance
    end

    def find_all_routes(orig, dest); end

    def build_route(nodes)
      route = GraphHelpers::Route.new
      build_route_helper(self, route, nodes)
    end

    def route_distance(nodes)
      build_route(nodes).distance
    rescue NoRouteFoundError
      'NO SUCH ROUTE'
    end

    def has_direct_path?(orig, dest)
      neighboring_nodes(orig).include?(dest)
    end

    def has_route?(orig, dest)
      get_routes(orig, dest).size.positive?
    end

    def retrieve_graph_nodes(graph, nodes_string)
      nodes = parse_new_nodes_string(nodes_string)
      graph.find_matching_nodes(nodes)
    end

    def parse_new_nodes_string(nodes_string)
      # format example: 'A, B, C'
      nodes_string.split(',').map(&:strip).collect { |node| Node.new(node) }
    end

    protected

    def build_route_helper(graph, route, nodes)
      return route if nodes.length <= 1

      edge = graph.find_edge(nodes[0], nodes[1])
      raise NoRouteFoundError if edge.nil?

      route.add_edge(graph.find_edge(nodes[0], nodes[1]))
      build_route_helper(graph, route, nodes[1..nodes.length])
    end
  end
end

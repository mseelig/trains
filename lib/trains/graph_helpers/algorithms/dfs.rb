# frozen_string_literal: true

require_relative '../route'
require 'set'

module Trains
  module GraphHelpers
    module Algorithms
      class InvalidOperatorError < RuntimeError; end

      class DFS
        attr_accessor :graph

        def initialize(graph)
          @graph = graph
        end

        def find_all_routes(orig, dest)
          collected_routes = []
          if orig == dest
            graph.neighboring_edges(orig).each do |e|
              neighboring_routes = []
              find_all_routes_helper(e.to, dest, Set.new, [], neighboring_routes)
              neighboring_routes.each do |r|
                r.prepend_edge(e)
              end
              collected_routes << neighboring_routes
            end
          else
            find_all_routes_helper(orig, dest, Set.new, [], collected_routes)
          end
          collected_routes.flatten
        end

        def find_all_routes_helper(o, d, visited, path, routes)
          visited.add o
          path.push o
          if o == d
            routes << graph.build_route(path) if path.length > 1
          else
            graph.neighboring_nodes(o).each do |n|
              find_all_routes_helper(n, d, visited, path, routes) unless visited.member?(n)
            end
          end
          path.pop
          visited.delete o
        end

        alias find_all_trips find_all_routes

        def find_all_cycle_trips(orig)
          find_all_trips(orig, orig)
        end

        def find_trips_by_exact_stops_with_cycles(orig, dest, num_stops)
          find_trips_by_method(orig, dest, num_stops, :stops, :==, true)
        end

        def find_trips_by_max_distance_with_cycles(orig, dest, _max_distance)
          find_trips_by_method(orig, dest, num_stops, :distance, :<=, true)
        end

        def find_trips_by_exact_distance(orig, dest, distance = nil)
          find_trips_by_method(orig, dest, distance, :distance, :==)
        end

        def find_trips_by_exact_stops(orig, dest, num_stops = nil)
          find_trips_by_method(orig, dest, num_stops, :stops, :==)
        end

        def find_trips_by_least_distance(orig, dest)
          find_all_routes(orig, dest).group_by(&:distance).min[1]
        end

        alias find_shortest_trips_by_distance find_trips_by_least_distance

        def find_trips_by_least_stops(orig, dest)
          find_all_routes(orig, dest).group_by(&:stops).min[1]
        end

        alias find_shortest_trips_by_stops find_trips_by_least_stops

        def find_trips_by_max_distance_with_cycles(orig, dest, max_distance = nil)
          find_trips_by_method(orig, dest, max_distance, :distance, :<=, true)
        end

        def find_trips_by_max_stops(orig, dest, max_stops = nil)
          find_trips_by_method(orig, dest, max_stops, :stops, :<=)
        end

        # operator must be one of <, <=, ==
        def find_trips_by_method(orig, dest, val, method, operator, include_cycles = false)
          raise ArgumentError unless val && method && operator
          raise InvalidOperatorError unless %i[< <= ==].include?(operator)

          trips_without_cycles = find_all_trips(orig, dest)
          filtered_trips_without_cycles = trips_without_cycles.select { |r| r.send(method).public_send(operator, val) }
          return filtered_trips_without_cycles unless include_cycles

          trips_without_cycles_copy = []
          trips_without_cycles.each { |t| trips_without_cycles_copy << t.copy } # needed since merge mutates routes
          all_cycle_trips = find_all_cycle_trips(dest).select { |a| a.send(method) <= val } # optimization
          # debug_var('trips_without_cycles', trips_without_cycles, :to_s)
          # debug_var('all_cycle_trips', all_cycle_trips, :to_s)
          trips_with_cycles = []
          trips_without_cycles.select { |r| r.send(method).public_send(:<=, val) }.each do |t|
            merge_trips_with_cycles_by_method(t, val - t.send(method), method, all_cycle_trips, trips_with_cycles)
          end
          [trips_without_cycles, trips_with_cycles].flatten.uniq.select do |t|
            t.send(method).public_send(operator, val)
          end
        end

        def merge_trips_with_cycles_by_method(trip, val, method, all_cycle_trips, collected_trips)
          # find all cycle_trips <= val, filtering by operator occurs in calling method
          # puts 'trip: ' + trip.to_s
          # puts 'val: ' + val.to_s
          filtered_trips = all_cycle_trips.select { |a| a.send(method) <= val }
          # puts 'filtered_trips: ' + filtered_trips&.map(&:to_s).join
          return if filtered_trips.empty?

          filtered_trips.each do |ft|
            # puts collected_trips.count
            new_trip = trip.copy
            collected_trips << [new_trip, new_trip.merge(ft)]
            merge_trips_with_cycles_by_method(new_trip, val - ft.send(method), method, all_cycle_trips, collected_trips)
          end
        end

        def debug_var(str, val, method)
          puts "#{str}: #{val.class}"
          val.each { |v| puts "  #{v.send(method)}" }
        end
      end
    end
  end
end

# frozen_string_literal: true

module Trains
  module GraphHelpers
    class EmptyRouteError < RuntimeError; end

    class IdenticalRouteError < RuntimeError; end

    class Route
      attr_accessor :edges_array

      def initialize(edges = nil)
        @edges_array = edges || []
      end

      # TODO: check if disconnected edge
      def add_edge(edge)
        edges_array << edge
        self
      end

      def copy
        Route.new(edges.collect(&:copy))
      end

      def dest
        raise EmptyRouteError if edges_array.empty?

        edges_array.last.to
      end

      def distance
        raise EmptyRouteError if edges_array.empty?

        edges_array.inject(0) { |sum, e| sum += e.distance }
      end

      alias edges edges_array

      def has_edge?(edge)
        edges_array.collect { |e| e == edge }.reduce(:|)
      end

      def hash
        node_path.hash
      end

      def merge(route)
        raise IdenticalRouteError if equal?(route)

        route.edges.each { |e| add_edge e }
        self
      end

      def node_path
        [orig.label, edges_array.collect { |e| e.to.label }].flatten
      end

      def orig
        raise EmptyRouteError if edges_array.empty?

        edges_array.first.from
      end

      # TODO: check if disconnected edge
      def prepend_edge(edge)
        edges_array.prepend(edge)
        self
      end

      def remove_last_edge
        raise EmptyRouteError if edges_array.empty?

        edges_array.pop
        self
      end

      def stops
        edges_array.count
      end

      def to_s
        "Route(#{stops}|#{distance}): " +
          orig.to_s +
          edges_array.collect { |e| "-#{e.to}" }.join
      end

      def eql?(other)
        self == other
      end

      def ==(other)
        edges.collect { |e| other.has_edge?(e) }.reduce(:|) &&
          edges.count == other.edges.count
      end

      private

      # not used
      def calc_and_update_distance
        @distance = edges_array.inject(0) { |sum, e| sum += e.distance }
      end
    end
  end
end

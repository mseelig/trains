# frozen_string_literal: true

module Trains
  # A node is a Multiton object where two nodes with the same label will be equal?
  class Node
    @@instances = []
    attr_accessor :label

    def self.new(*a, &b)
      multiton = @@instances.select { |i| i.label == a[0] }.first
      if multiton.nil?
        obj = allocate
        @@instances << obj
        obj.send(:initialize, *a, &b)
        obj
      else
        multiton
      end
    end

    def initialize(label)
      @label = label
    end

    def to_s
      label
    end

    # NOTE: the following shouldn't be required since the class only stores unique instances
    def ==(other)
      label == other.label
    end

    def equal?(other)
      label == other.label
    end
  end
end

# frozen_string_literal: true

require 'test_helper'
require_relative '../lib/trains/base'

class AlgorithmsTest < Minitest::Test
  extend Minitest::Spec::DSL

  let(:graph) { Graph.new.tap { |obj| obj.add_edges 'AB2, BC3' } }
  let(:node_A) { Node.new 'A' }
  let(:node_B) { Node.new 'B' }
  let(:node_C) { Node.new 'C' }
end

# frozen_string_literal: true

require 'test_helper'
require_relative '../lib/trains/base'

class EdgeTest < Minitest::Test

  def setup
    @node_A = Node.new 'A'
    @node_B = Node.new 'B'
    @node_C = Node.new 'C'
    @edge_AB2 = Edge.new 'AB2'
    @edge_BC3 = Edge.new 'BC3'
    @edge_AB5 = Edge.new 'AB5'
  end

  def test_edge
    assert_raises(ArgumentError) { Edge.new }
    assert_raises(EdgeParseError) { Edge.new 'BC' }
    assert_raises(ZeroDistanceError) { Edge.new 'BC0' }
    assert_equal(@node_A, @edge_AB2.from)
    assert_equal(@node_B, @edge_AB2.to)
    assert_equal(2, @edge_AB2.distance)
    refute(@edge_AB2 == @edge_BC3)
    assert(@edge_AB2 == @edge_AB5) # disregard distance for ==
  end
end

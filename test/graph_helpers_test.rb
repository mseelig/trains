# frozen_string_literal: true

require 'test_helper'
require_relative '../lib/trains/base'

class GraphHelpersTest < Minitest::Test
  extend Minitest::Spec::DSL

  let(:graph) { Graph.new.tap { |obj| obj.add_edges 'AB2, BC3' } }
  let(:node_A) { Node.new 'A' }
  let(:node_B) { Node.new 'B' }
  let(:node_C) { Node.new 'C' }

  def test_build_route
    assert_raises(NoRouteFoundError) { graph.build_route([node_A, node_C]).distance }
    graph.add_edge(Edge.new('AC5'))
    assert_equal(5, graph.build_route([node_A, node_C]).distance)
  end

  def test_direct_path_length
    assert_equal(2, graph.direct_path_length(node_A, node_B))
    assert_equal(3, graph.direct_path_length(node_B, node_C))
    assert_raises(NoDirectPathError) { graph.direct_path_length(node_A, node_C) }
    graph.add_edge(Edge.new('AC5'))
    assert_equal(5, graph.direct_path_length(node_A, node_C))
  end

  def test_find_routes; end

  def test_has_direct_path?
    assert(graph.has_direct_path?(node_A, node_B))
    assert(graph.has_direct_path?(node_B, node_C))
    refute(graph.has_direct_path?(node_A, node_C))
  end

  def test_has_route?
    # assert(graph.has_route?(node_A, node_C)) # A->B->C
    # refute(graph.has_route?(node_C, node_A))
    # graph.add_edge('CA5')
    # assert(graph.has_route?(node_C, node_A))
  end
end

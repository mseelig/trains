# frozen_string_literal: true

require 'test_helper'
require_relative '../lib/trains/base'

include GraphHelpers

class GraphTest < Minitest::Test

  def setup
    @empty_graph = Graph.new
    @node_A = Node.new 'A'
    @node_B = Node.new 'B'
    @node_C = Node.new 'C'
    @edge_AB2 = Edge.new 'AB2'
    @edge_BC3 = Edge.new 'BC3'
    @edge_AB5 = Edge.new 'AB5'
    @edge_AC4 = Edge.new 'AC4'
  end

  def test_add_edge
    g = @empty_graph
    assert_equal(0, g.nodes.count)
    assert_equal(0, g.edges.count)
    assert(g.empty?)
    g.add_edge @edge_AB2
    assert_equal(2, g.nodes.count)
    assert_equal(1, g.edges.count)
    g.add_edge @edge_BC3
    assert_equal(3, g.nodes.count)
    assert_equal(2, g.edges.count)
    assert_raises(DuplicateEdgeError) { g.add_edge @edge_AB5 }
  end

  def test_add_edges
    skip 'TODO: should accept array of edges instead of edge string'
  end

  def test_add_node
    g = Graph.new
    assert_equal(0, g.size)
    g.add_node(@node_A)
    assert_equal(1, g.size)
    assert(g.has_node?(@node_A))
    assert(0, g.edges.count)
  end

  def test_add_nodes
    g = Graph.new
    assert_equal(0, g.size)
    g.add_nodes([@node_A, @node_B])
    assert_equal(2, g.size)
    assert(g.has_node?(@node_A))
    assert(g.has_node?(@node_B))
    assert(0, g.edges.count)
  end

  def test_find_edge
    g1 = @empty_graph
    assert_nil(g1.find_edge(@node_A, @node_B))
    g2 = Graph.new.tap { |obj| obj.add_edges 'AB2, BC3' }
    assert_equal(2, g2.find_edge(@node_A, @node_B).distance)
    assert_equal(3, g2.find_edge(@node_B, @node_C).distance)
    assert_nil(g2.find_edge(@node_A, @node_C))
  end

  def test_has_node
    g = Graph.new
    g.add_edge(@edge_AB2)
    assert(g.has_node?(@node_A))
    assert(g.has_node?(@node_B))
    refute(g.has_node?(@node_C))
  end

  def test_neighboring_edges
    g = Graph.new
    g.add_edge(@edge_AB2)
    g.add_edge(@edge_BC3)
    g.add_edge(@edge_AC4)
    assert_equal([@edge_AB2, @edge_AC4], g.neighboring_edges(@node_A))
  end

  def test_neighboring_nodes
    g = Graph.new
    g.add_edge(@edge_AB2)
    g.add_edge(@edge_BC3)
    g.add_edge(@edge_AC4)
    assert_equal([@node_B, @node_C], g.neighboring_nodes(@node_A))
  end

  def test_size
    g = Graph.new
    g.add_edge(@edge_AB2)
    g.add_edge(@edge_BC3)
    assert_equal(3, g.size) # nodes
  end
end

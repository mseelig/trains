# frozen_string_literal: true

require 'test_helper'
require_relative '../lib/trains/base'

class NodeTest < Minitest::Test
  def test_node
    assert_raises(ArgumentError) { Node.new }
    n1 = Node.new 'A'
    assert_equal('A', n1.label)
    assert_equal('A', n1.to_s)
    n2 = Node.new 'B'
    refute(n1 == n2)
    n3 = Node.new 'A'
    assert(n1 == n3)
    assert(n1.equal?(n3))
  end
end

# frozen_string_literal: true

require 'test_helper'
require_relative '../lib/trains/base'

EDGES_STRING = 'AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7'

class QuestionsTest < Minitest::Test
  extend Minitest::Spec::DSL

  let(:graph) { Graph.new.tap { |obj| obj.add_edges EDGES_STRING } }
  let(:node_A) { Node.new 'A' }
  let(:node_B) { Node.new 'B' }
  let(:node_C) { Node.new 'C' }
  let(:node_D) { Node.new 'D' }
  let(:node_E) { Node.new 'E' }
  let(:dfs) { GraphHelpers::Algorithms::DFS.new graph }

  def test_questions
    # question 1
    assert_equal(9, graph.route_distance([node_A, node_B, node_C]))
    # question 2
    assert_equal(5, graph.route_distance([node_A, node_D]))
    # question 3
    assert_equal(13, graph.route_distance([node_A, node_D, node_C]))
    # question 4
    assert_equal(22, graph.route_distance([node_A, node_E, node_B, node_C, node_D]))
    # question 5
    assert_equal('NO SUCH ROUTE', graph.route_distance([node_A, node_E, node_D]))
    # question 6
    assert_equal(2, dfs.find_trips_by_method(node_C, node_C, 3, :stops, :<=).count)
    # question 7
    assert_equal(3, dfs.find_trips_by_method(node_A, node_C, 4, :stops, :==, true).count)
    # question 8
    assert_equal(9, dfs.find_trips_by_least_distance(node_A, node_C).sample&.distance)
    # question 9
    assert_equal(9, dfs.find_trips_by_least_distance(node_B, node_B).sample&.distance)
    # question 10
    assert_equal(7, dfs.find_trips_by_method(node_C, node_C, 30, :distance, :<, true).count)
  end
end

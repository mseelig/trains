# frozen_string_literal: true

require 'test_helper'
require_relative '../lib/trains/base'

class RouteTest < Minitest::Test
  include GraphHelpers

  def setup
    @empty_route = Route.new
    @node_A = Node.new 'A'
    @node_B = Node.new 'B'
    @node_C = Node.new 'C'
    @edge_AB1 = Edge.new 'AB1'
    @edge_BC2 = Edge.new 'BC2'
    @route_with_edges = Route.new [@edge_AB1, @edge_BC2]
  end

  def test_distance
    assert_raises(EmptyRouteError) { @empty_route.distance }
    assert_equal(3, @route_with_edges.distance)
  end

  def test_add_edge
    r = @empty_route.add_edge @edge_AB1
    assert_equal(1, r.distance)
    r.add_edge @edge_BC2
    assert_equal(3, r.distance)
  end

  def test_copy
    r = @route_with_edges.copy
    assert(r.eql? @route_with_edges)
    refute(r.equal? @route_with_edges)
    assert(r.edges == @route_with_edges.edges)
    assert_equal(r.distance, @route_with_edges.distance)
    assert_equal(r.stops, @route_with_edges.stops)
    assert(r == @route_with_edges)
  end

  def test_dest
    assert_raises(EmptyRouteError) { @empty_route.dest }
    assert_equal(@node_C, @route_with_edges.dest)
  end

  def test_distance
    assert_raises(EmptyRouteError) { @empty_route.distance }
    assert_equal(3, @route_with_edges.distance)
  end

  def test_merge
    r1 = Route.new [@edge_AB1]
    assert_raises(IdenticalRouteError) { r1.merge(r1) }
    r2 = Route.new [@edge_BC2]
    r3 = r1.merge(r2)
    assert_equal(3, r3.distance)
    assert_equal(2, r3.stops)
  end

  def test_node_path
    assert_equal(['A', 'B', 'C'], @route_with_edges.node_path)
  end

  def test_prepend_edge
    r = Route.new [@edge_BC2]
    assert_equal(@node_B, r.orig)
    r.prepend_edge @edge_AB1
    assert_equal(@node_A, r.orig)
  end

  def test_remove_last_edge
    assert_equal(@node_C, @route_with_edges.dest)
    @route_with_edges.remove_last_edge
    assert_equal(@node_B, @route_with_edges.dest)
  end

  def test_orig
    assert_raises(EmptyRouteError) { @empty_route.dest }
    assert_equal(@node_C, @route_with_edges.dest)
  end

  def test_stops
    skip 'TODO: should return EmptyRouteError for empty route'
    assert_raises(EmptyRouteError) { @empty_route.stops }
    assert_equal(2, @route_with_edges.stops)
  end
end
